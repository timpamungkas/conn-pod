package com.jaeger.connpod.rest.json;

import java.util.Arrays;
import java.util.List;

public class JsonBaseResponse<T> {

	private JsonBaseHeader header;

	private T data;

	public JsonBaseResponse() {
		super();
	}

	public JsonBaseResponse(T data, long startTime, boolean success, JsonBaseError... errors) {
		this(data, startTime, success, Arrays.asList(errors));
	}

	public JsonBaseResponse(T data, long startTime, boolean success, List<JsonBaseError> errors) {
		this(startTime, success, errors);
		this.data = data;
	}

	public JsonBaseResponse(long startTime, boolean success, JsonBaseError... errors) {
		this(startTime, success, Arrays.asList(errors));
	}

	public JsonBaseResponse(long startTime, boolean success, List<JsonBaseError> errors) {
		this();

		JsonBaseHeader jsonBaseHeader = new JsonBaseHeader();
		jsonBaseHeader.setSuccess(success);
		jsonBaseHeader.setErrors(errors);
		jsonBaseHeader.setProcessTimeMillis(System.currentTimeMillis() - startTime);
		this.setHeader(jsonBaseHeader);
	}

	public T getData() {
		return data;
	}

	public JsonBaseHeader getHeader() {
		return header;
	}

	public void setContent(T data) {
		this.data = data;
	}

	public void setHeader(JsonBaseHeader header) {
		this.header = header;
	}

}
