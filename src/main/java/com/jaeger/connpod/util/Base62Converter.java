package com.jaeger.connpod.util;

/**
 * A Base10-Base62 converter. Original source in
 * <a href="https://gist.github.com/jdcrensh/4670128">here</a>
 *
 * @author timpamungkas
 */
public class Base62Converter {

	// sequence is important!
	public static final String BASE62_CHARS = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";

	public static final int BASE = BASE62_CHARS.length();

	public String fromBase10(long i) {
		StringBuilder sb = new StringBuilder("");
		if (i == 0) {
			return Character.toString(BASE62_CHARS.charAt(0));
		}
		while (i > 0) {
			i = fromBase10(i, sb);
		}
		return sb.reverse().toString();
	}

	private long fromBase10(final long i, final StringBuilder sb) {
		int rem = (int) (i % BASE);
		sb.append(BASE62_CHARS.charAt(rem));
		return i / BASE;
	}

	private long toBase10(final char[] chars) {
		long n = 0;
		for (int i = chars.length - 1; i >= 0; i--) {
			n += toBase10(BASE62_CHARS.indexOf(chars[i]), i);
		}
		return n;
	}

	private long toBase10(final int n, final int pow) {
		return n * (long) Math.pow(BASE, pow);
	}

	public long toBase10(final String str) {
		return toBase10(new StringBuilder(str).reverse().toString().toCharArray());
	}

}