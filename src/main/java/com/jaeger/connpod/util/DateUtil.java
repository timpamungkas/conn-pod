package com.jaeger.connpod.util;

import java.util.Calendar;
import java.util.Date;

public class DateUtil extends org.apache.commons.lang3.time.DateUtils {

	public static final String DATE_FORMAT = "yyyy-MM-dd";
	public static final String DATE_TIME_FORMAT = "yyyy-MM-dd'T'HH:mm:ss";
	public static final String DATE_TIME_FORMAT_WITH_MILLISECOND = "yyyy-MM-dd'T'HH:mm:ss.SSS";
	public static final Date DATE_ETERNITY;

	static {
		Calendar c = Calendar.getInstance();
		c.set(2099, 11, 31);

		DATE_ETERNITY = c.getTime();
	}
}
