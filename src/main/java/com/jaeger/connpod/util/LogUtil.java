package com.jaeger.connpod.util;

import org.apache.commons.lang3.StringUtils;

public class LogUtil {

	private static final String DATA_DELIMITER = "||";

	public String generateLogText(long processTimeMillis, String... additionalData) {
		StringBuilder sb = new StringBuilder();
		sb.append(" timeMillis:[" + processTimeMillis + "] data:");

		for (int i = 0; i < additionalData.length; i++) {
			if (!StringUtils.isBlank(additionalData[i])) {
				sb.append(additionalData[i]);
				if (i < additionalData.length - 1 && !StringUtils.isEmpty(additionalData[i + 1])) {
					sb.append(DATA_DELIMITER);
				}
			}
		}

		return sb.toString();
	}

}
