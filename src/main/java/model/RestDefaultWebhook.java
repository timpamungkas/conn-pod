package model;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;

public class RestDefaultWebhook {

	@JsonProperty("process_time")
	private Date processTime;

	private String message;

	private String uuid;

	public String getMessage() {
		return message;
	}

	public Date getProcessTime() {
		return processTime;
	}

	public String getUuid() {
		return uuid;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public void setProcessTime(Date processTime) {
		this.processTime = processTime;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

}
